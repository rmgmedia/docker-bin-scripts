RMG Docker Bin Scripts
======================

A collection of minimally opinionated bash scripts for use on RMG Media Docker projects to 
reduce Docker Compose knowledge needed in order to do development.

## Command List

- `bash`: Convenience method that runs `bash` in a Docker container. This puts you into
  a bash shell for that container.
- `cli`: Runs a command in a Docker container.
- `deploy-app`: Opinionated app deployer. Assumes convention of having a meta container 
  for an app like "magento" and a workspace container like "magento-workspace" which has
  a `$HOME/deploy-app.sh` file. Run like `bin/deploy-app react` or 
  `bin/deploy-app magento`.
- `down`: Stops and removes Docker containers.
- `logs`: Tails Docker container logs. Can pass a `-f` argument to follow logs. `-t` to
  show timestamps.
- `status`: Checks status of Docker containers.
- `up`: Creates and starts Docker containers.

## Notes
- All commands must be run from the root of the Docker project.
- Most commands accept containers as unnamed arguments which determine which Docker
  containers the command will be run against. This name comes from the key for the
  container in the `docker-composer.yaml` file.

