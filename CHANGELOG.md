# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.1.5] - 2022-07-31

### Changed

- Added `--no-cache` and `--b|--build` flags to `up` command.
- Changed default behavior of `up` to not build to mirror behavior of `docker-compose up`.

### Fixed
- Minor code cleanup
- Fixed bug in `bash` when passing `-u|--user`

## [5.1.4] - 2022-07-28

### Fixed

- Replaced `/usr/bin/env bash` calls with `/bin/bash`. The former was causing issues.

## [5.1.3] - 2022-07-27

### Changed

- Added `-v|--volumes` flag to `down` command.

### Deprecated

- Deprecated the `cert-new`, `cli`, and `deploy-app` commands.

## [5.1.2] - 2021-08-17

### Fixed

- Fixed cert-new to avoid failing with exit code 1

## [5.1.1] - 2021-07-29

### Fixed

- Fixed typo in cert-new
- Updated composer.json to include cert-new

## [5.1.0] - 2021-07-27

### Added

- Added cert-new

## [5.0.1] - 2021-01-09

### Changed

- Removed bin/up call from bin/deploy-app

## [5.0.0] - 2020-11-15

### Removed

- Removed `destroy` command.
- Removed `restart` command.
- Removed `start` command.
- Removed `stop` command.
- Removed `update` command.

### Added

- Added `deploy-app` command.
- Added `down` command.
- Added `up` command.

### Changed

- Updated command convention for accepting multiple container arguments to read all
  unnamed arguments as containers. Command `cli` was not changed.

## [4.0.0] - 2020-10-28

### Changed

- Updated package name from `rmgmedia/docker-bin-scripts` to `rmg/docker-bin-scripts`.
- Replaced Linux `getopt` with more portable options parsing.
- Updated `CHANGELOG` format to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Removed

- Removed `install` command.

## [3.2.4] - 2020-10-28

### Changed

- Removed requirement for `realpath` command since it requires `coreutils` package
  to be installed.

## [3.2.3] - 2020-10-28

### Fixed

- Updated docker bin scripts to use bash from env.

## [3.2.2] - 2020-10-15

### Fixed

- Fixed inconsistencies with `update` command when container argument given vs no given.

## [3.2.1] - 2020-10-15

### Fixed

- Added default for `$TIMESTAMPS` variable in `logs` command.

## [3.2.0] - 2020-10-15

### Added

- Added `-t|--timestamp` argument for `logs` command.

## [3.1.1] - 2020-09-29

### Fixed

- Fixed `restart` command inclusion in list of commands in vendor/bin.

## [3.1.0] - 2020-09-28

### Added

- Added `restart` command.

### Removed

- Removed support for `$DEFAULT_CONTAINER` env variable. Made
  setting of container always explicit.

## [2.0.0] - 2020-09-10

### Removed

- Removed support for old way of handling docker config files.

## [1.3.2] - 2020-09-10

### Added

- Added `status` command to list of commands.

### Fixed

- Fixed `install` command after `start` command change.

## [1.3.1] - 2020-09-10

### Fixed

- Fixed `start` command. Had accidentally copy/pasted exactly from `stop` command.

## [1.3.0] - 2020-09-10

### Added

- Added `status` command.
- Added `--container` argument to Updated `start` and `stop` comments.

### Fixed

- Removed unecessary rm command in `update`.
- Updated to start only the specific container if given as argument.

## [1.2.1] - 2020-03-29

### Fixed

- Fixed `cli` command with multiple commands.
- Fixed default value for `$DEFAULT_CONTAINER` in `cli` command.

## [1.2.0] - 2020-02-23

### Added

- Added reading of `install.sh` files from within ./docker/ folders

## [1.1.1] - 2020-01-11

### Fixed

- Fixed syntax error in `update` command.

## [1.1.0] - 2020-01-11

### Added

- Added ability to specify a container with `update` command.

### Changed

- Updated bash ! -z syntax for tests with -n.

## [1.0.1] - 2019-12-25

### Fixed

- Fixed issue with running `install.sh` on new setup.

## [1.0.0] - 2019-10-27

### Added

- Initial release
